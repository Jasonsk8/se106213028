<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>無標題文件</title>
</head>

<body>
<?php
	require('dbconfig.php');//

	$A=$_POST['name'];
	$B=$_POST['price'];
	$C=$_POST['descrip'];

	if(!ctype_digit($B)){ // 0以上整數函數
		echo "<h1 align=center>新增失敗 Price不是正整數 😱😱</h1>";
		echo "<br><br>";
		goto END;
	}

	if(isset($_POST["action"]) && ($_POST["action"] == "add")) {//make sure manager click "add" button
		$sql = "INSERT INTO product(name, price, detail) VALUES(?, ?, ?)";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "sis", $A, $B, $C); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		echo "<h1 align=center>新增成功 👍👍👍</h1>";
		echo "<br><br>";
	}
	
	END:
		echo "<h2 align=center><a href=main.php>回主畫面</a></h2>"
?>

</body>
</html>
