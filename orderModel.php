<?php
require_once("dbconfig.php");

function getOrderList($uID) {//用user找訂單資料
	global $db;
	$sql = "SELECT ordID, orderDate, status FROM userOrder WHERE uID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getOrderList2() {//找order詳細資料
	global $db;
	$sql = "SELECT ordID, orderDate, status FROM userOrder WHERE ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $_GET["ordID"]); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getOrderListAdmin($uID) {//工作人員在用的，抓出所有訂單，可以讓工作人員跟改status
	global $db;
	$sql = "SELECT ordID, orderDate, status FROM userOrder";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function _getCartID($uID) {//看未結帳的order
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "insert into userOrder ( uID, status ) values (?,0)";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}

function _getCartID2($uID) {
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	}
}

function addToCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "insert into orderItem (ordID, prdID, quantity) values (?,?,1);";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function removeFromCart($uID, $serno) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "delete from orderItem where serno=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $serno); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function checkout($ordID, $uID, $address) {
	global $db;
	$ordID= _getCartID2($uID);
	$sql = "UPDATE `userorder` SET orderDate = current_timestamp(), status = 1 , address =? where uID =? AND ordID =?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ssi", $address, $uID, $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getOrderDetail($ordID) {
	global $db;
	$sql="select orderItem.prdID, product.name, product.price, orderItem.quantity from userOrder, product, orderItem WHERE orderItem.prdID=product.prdID and orderItem.ordID=? and orderItem.ordID=userOrder.ordID";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

?>










