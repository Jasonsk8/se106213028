<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
</head>
<body>
<p>This is the MAIN page</p>
<hr>
Your Login status: 
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"], ", Your ID is: ", $_SESSION["loginProfile"]["uID"],"<hr>";
	$result=getOrderList2($_SESSION["loginProfile"]['uID']);
?>
	<table width="350" border="1">
  <tr>
    <td align=center>order ID</td>
    <td align=center>Date</td>
    <td align=center>Status</td>
  </tr>
<?php
while (	$rs=mysqli_fetch_assoc($result)) {//抓表格資料
	echo "<tr><td align=center>" . $rs['ordID'] . "</td>";
	echo "<td align=center>{$rs['orderDate']}</td>";
	echo "<td align=center>" , $rs['status'], "</td>";
	echo "</tr>";
}
?>
</table>
<table width="350" border="1">
  <tr>
	<td align=center>prdID</td>
	<td align=center>prdName</td>
	<td align=center>Unit Price</td>
	<td align=center>quantity</td>
	<td align=center>Total Price</td>
  </tr>
<?php
$OrderID=$_GET['ordID'];
$result2 = getOrderDetail($OrderID);
$total = 0;
$tot = 0;
while (	$rs2=mysqli_fetch_assoc($result2)) {
	echo "<tr><td align=center>" . $rs2['prdID'] . "</td>";
	echo "<td align=center>{$rs2['name']}</td>";
	echo "<td align=center>" , $rs2['price'], "</td>";
	echo "<td align=center>" , $rs2['quantity'], "</td>";
	$total = $rs2['quantity'] * $rs2['price'];
	$tot += $total;
	echo "<td align=center>" , $total , "</td>";
	echo "</tr>";
}
echo "<tr><td colspan=5 align=right>Total Price of the List : $tot</td></tr>";
?>

</table>
<a href="showOrders.php">OK</a><hr>

</body>
</html>
