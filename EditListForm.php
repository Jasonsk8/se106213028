<?php
	session_start();
	require("prdModel.php");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Triangle</title>
<!-- CSS -->
<style type="text/css">
body{
    font-family: "微軟正黑體"; 
    font-size: 20px;
}
table {
    box-sizing: border-box;
	padding: 20px 20px 20px 20px;
	width:500px;
	margin: 20px auto;
	line-height:30px;
	background: linear-gradient(to right, #ffe8df, #fcc8ef, #efc8cd, #ffa8cd, #efc8cd, #fcc8ef, #ffe8df);
	background-position:center;
	font-size:17pt;
}
</style>
<!-- CSS End -->
</head>
<body>
<h1 align="center">👉 &nbsp 修改商品資料 &nbsp 👈</h1>
<p align="center"><a href="main.php">回主畫面</a></p>
<?php
	$result=getPrdList2();
	$rs=mysqli_fetch_assoc($result);
?>
<?php echo "<form action='update.php?prdID=" , $rs['prdID'] , "' method=post name=listUpdate id=listUpdate>" ?>
<table border="3" align="center" cellpadding="4">
	<tr>
		<th>&nbsp 欄位 &nbsp</th>
		<th>&nbsp 商品內容 &nbsp</th>
	</tr>
	
	<tr>
		<td align="center">prdID</td>
		<td align="center"><?php echo $rs['prdID'];?></td>
	</tr>
	<tr>
		<td align="center">Name</td>
		<td><input type="text" name="name" id="name" value="<?php echo $rs['name'];?>"></td>
	</tr>
	<tr>
		<td align="center">Price</td>
		<td><input type="text" name="price" id="price" value="<?php echo $rs['price'];?>"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<input name="action" type="hidden" value="update">
		<input type="submit" value="修改內容">
		<input type="reset" value="重新填寫"></td>
	</tr>

</table>
</form>
</body>
</html>