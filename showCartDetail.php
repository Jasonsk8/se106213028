<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Examples</title>
</head>
<body>
<p>This is the Shopping Cart Detail 
[<a href="logout.php">logout</a>]
</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getCartDetail($_SESSION["loginProfile"]["uID"]);
?>
<a href="showOrders.php">List My Cart Items</a><hr>
	<table width="450" border="1">
  <tr>
    <td align=center>id</td>
    <td align=center>Prd Name</td>
    <td align=center>price</td>
    <td align=center>Quantity</td>
    <td align=center>Amount</td>
	<td align=center>-</td>
  </tr>
<?php
$total=0;
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<tr><td align=center>" . $rs['serno'] . "</td>";
	echo "<td align=center>{$rs['name']}</td>";
	echo "<td align=center>" , $rs['price'], "</td>";
	echo "<td align=center>" , $rs['quantity'], "</td>";
	$total += $rs['quantity'] *$rs['price'];
	echo "<td align=center>" , $rs['quantity'] *$rs['price'] , "</td>";
	echo "<td align=center><a href='removeFromCart.php?serno=" , $rs['serno'] , "'>Remove</a></td></tr>";
}
echo "<tr><td colspan=6 align=right>Total Price is: $total</td></tr>";
?>
</table>
<hr>
<a href="confirmOrder.php">Yes Checkout</a>  <a href="main.php">No, keep shopping</a>
</body>
</html>
